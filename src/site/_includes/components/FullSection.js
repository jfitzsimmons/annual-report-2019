module.exports = (content = "", classes = "", title = "") => `
  <section class="${classes}">
    <h4 class="ttu tracked tc b f6 ma0 whitney">${title}</h4>

      ${content}
    
  </section>
`;
