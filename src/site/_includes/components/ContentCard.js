module.exports = ({
  src,
  title = "",
  teaser = "",
  video = "",
  starttime = ""
} = {}) => `
  <div class="content-card" tabindex="0">
    <div class="content-card__image" ${video ? `data-video="${video}"` : ``}${
  starttime ? ` data-start-time="${starttime}"` : ``
}>
      <img src="${src}" alt="">
      ${
        video
          ? `<div class="play-video"><div class="play-video-inner"><img alt="" src="../img/play-white.svg"><p class="ttu tracked f7 b tc">Play video</p></div></div>`
          : ``
      }      
    </div>
    <div class="pa4 content-card__content">
      <h3 class="mt0 f6">${title}</h3>
      <p class="f6">${teaser}</p>
    </div>
  </div>
  `;
