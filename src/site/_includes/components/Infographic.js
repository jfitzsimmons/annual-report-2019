module.exports = ({ src, number = "", caption = "", link = "" } = {}) => `
  <a href="${link}" target="_blank" class="infographic flex items-stretch db">
    <div class="center flex items-end" style="background-image: url(${src})">
      <div class="infographic-content">
        <p class="ma0 f3 b lh-solid gotham">${number}</p>
        <p class="mt2 mb0 lh-title">${caption}</p>
      </div>
        ${link ? `<div class="ib button btn-white">More</div>` : ``}      
    </div>
  </a>
  `;
