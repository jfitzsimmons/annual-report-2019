module.exports = (content = "", classes = "") => `
  <section class="${classes}">
    <div class="content-container">
      ${content}
    </div>
  </section>
`;
