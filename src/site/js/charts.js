Chart.defaults.global.defaultFontFamily = '"Whitney SSm A", "Whitney SSm B"';

var endowment = $("#endowment");

var myLineChart = new Chart(endowment, {
  type: "line",
  options: {
    tooltips: {
      callbacks: {
        label: function(tooltipItems, data) {
          return (
            "$" + data.datasets[0].data[tooltipItems.index].toLocaleString()
          );
        }
      }
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: false,
            callback: function(value, index, values) {
              return "$" + value.toLocaleString().split(",")[0] + " mil";
            }
          }
        }
      ]
    },
    legend: {
      display: false
    },
    title: {
      display: true,
      text: "10-year Growth of Endowment"
    }
  },
  data: {
    labels: [
      "2010",
      "2011",
      "2012",
      "2013",
      "2014",
      "2015",
      "2016",
      "2017",
      "2018",
      "2019"
    ],
    datasets: [
      {
        borderColor: "#ffcc33",
        data: [
          9328147,
          11513827,
          12006461,
          15034823,
          18608240,
          22732394,
          29481008,
          33616162,
          37417888,
          41849759
        ],
        fill: false
      }
    ]
  }
});

var scholarshiprecipients = $("#scholarshiprecipients");

var myLineChart = new Chart(scholarshiprecipients, {
  type: "line",
  options: {
    legend: {
      display: false
    },
    title: {
      display: true,
      text: "Number of Donor Funded Student Scholarship Recipients"
    }
  },
  data: {
    labels: ["2014", "2015", "2016", "2017", "2018", "2019"],
    datasets: [
      {
        borderColor: "#235937",
        data: [277, 290, 320, 365, 433, 466],
        fill: false
      }
    ]
  }
});

var options = {
  cutoutPercentage: 75,
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  legendCallback: function(chart) {
    var text = [];
    text.push('<ul class="' + chart.id + '-legend">');
    for (var i = 0; i < chart.data.datasets[0].data.length; i++) {
      text.push(
        '<li style="display: flex; align-items: center; font-size: 1rem"><span style="background-color:' +
          chart.data.datasets[0].backgroundColor[i] +
          '; width: 1rem; height: 1rem; display: inline-block; margin-right: .5rem;">'
      );
      text.push("</span>");
      if (chart.data.labels[i]) {
        text.push(
          chart.data.labels[i] + " (" + chart.data.datasets[0].data[i] + "%)"
        );
      }
      text.push("</li>");
    }
    text.push("</ul>");
    return text.join("");
  },
  tooltips: {
    enabled: true,
    callbacks: {
      label: function(tooltipItems, data) {
        return (
          data.labels[tooltipItems.index] +
          " (" +
          data.datasets[0].data[tooltipItems.index] +
          "%)"
        );
      }
    }
  }
};

var ctx = $("#collegeschool");
var myPieChart = new Chart(ctx, {
  type: "pie",
  data: {
    labels: [
      "College of Liberal Arts and Sciences",
      "School of Business",
      "School of Education",
      "School of Communication, Media and the Arts",
      "Undeclared"
    ],
    datasets: [
      {
        backgroundColor: [
          "#d3852e",
          "#494e89",
          "#0f83b3",
          "#ffcc33",
          "#2a6143"
        ],
        data: [40, 17, 14, 13, 16]
      }
    ]
  },
  options: options
});

$("#collegeschool-legend").html(myPieChart.generateLegend());

var revData = {
  datasets: [
    {
      data: [20.9, 42.9, 16.7, 17.8, 1.7],
      backgroundColor: ["#d3852e", "#494e89", "#0f83b3", "#ffcc33", "#2a6143"]
    }
  ],
  labels: [
    "Tuition and fees (net of aid)",
    "General state appropriations",
    "Auxiliary enterprises",
    "Grants and contracts",
    "Capital and other funds"
  ]
};

/*
 * Check to see if there's a div with an id of "exp" which means we are on the FF page.
 * Set the values (in %), colors, and labels for the Expenditures chart here
 */
var expData = {
  datasets: [
    {
      data: [40.3, 10.8, 11.4, 18.3, 6.0, 9.5, 3.7],
      backgroundColor: [
        "#d3852e",
        "#494e89",
        "#0f83b3",
        "#ffcc33",
        "#2a6143",
        "#AB2600",
        "#cecece"
      ]
    }
  ],
  labels: [
    "Instruction",
    "Institutional support",
    "Academic support",
    "Housing, dining and other auxiliary",
    "Scholarships",
    "Student services",
    "Research and public service"
  ]
};

var expctx = $("#exp");
var expChart = new Chart(expctx, {
  type: "doughnut",
  data: expData,
  options: options
});
$("#exp-legend").html(expChart.generateLegend());

var revctx = $("#rev");
var revChart = new Chart(revctx, {
  type: "doughnut",
  data: revData,
  options: options
});
$("#rev-legend").html(revChart.generateLegend());
