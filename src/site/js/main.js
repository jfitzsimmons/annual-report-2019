document.addEventListener("DOMContentLoaded", function(event) {
  if (document.querySelectorAll(".video--callout iframe").length) {
    var video = new Vimeo.Player(
      document.querySelectorAll(".video--callout iframe")[0]
    );
  }

  if (video) {
    $(".video--callout").append(
      '<button class="video-controls" aria-label="Toggle video play/pause"><svg class="icon icon-pause2">\n' +
        '  <use xlink:href="../img/symbol-defs.svg#icon-pause2"></use>\n' +
        "</svg> </button>"
    );
  }

  var paused = false;

  if (Cookies.get("reduced-motion") == "true") {
    paused = true;
  }

  if (paused) {
    if (video) {
      video.on("play", function() {
        video.pause();
      });
    }
    $(".homepage-hero .blank").addClass("paused");
    $("button.video-controls").each(function() {
      $(this).html(
        '<svg class="icon icon-play4">\n' +
          '  <use xlink:href="/annual-report-2019/img/symbol-defs.svg#icon-play4"></use>\n' +
          "</svg> "
      );
    });
  }

  $("button.video-controls").click(function() {
    if (!paused) {
      if (video) {
        video.on("play", function() {
          video.pause();
        });
        video.pause();
      }
      $("button.video-controls").html(
        '<svg class="icon icon-play4">\n' +
          '  <use xlink:href="/annual-report-2019/img/symbol-defs.svg#icon-play4"></use>\n' +
          "</svg> "
      );
      $(".homepage-hero .blank").addClass("paused");
      Cookies.set("reduced-motion", "true");
      paused = true;
    } else {
      if (video) {
        video.off("play");
        video.play();
      }

      $("button.video-controls").html(
        '<svg class="icon icon-pause2">\n' +
          '  <use xlink:href="../img/symbol-defs.svg#icon-pause2"></use>\n' +
          "</svg> "
      );
      $(".homepage-hero .blank").removeClass("paused");
      Cookies.set("reduced-motion", "false");
      paused = false;

      initWords();
      StartTextAnimation(0);
    }
  });

  var dataText;

  function initWords() {
    // array with texts to type in typewriter
    dataText = [];
    dataText = $(".blank")
      .text()
      .split(", ");
  }

  function breakWords() {
    dataText = dataText.join(", ");
    $(".blank").text(dataText);
  }

  // type one text in the typwriter
  // keeps calling itself until the text is finished
  function typeWriter(text, i, fnCallback) {
    // chekc if text isn't finished yet
    if (i < text.length) {
      // add next character to h1
      document.querySelector(".blank").innerHTML =
        text.substring(0, i + 1) + '<span aria-hidden="true"></span>';

      // wait for a while and call this function again for next character
      setTimeout(function() {
        typeWriter(text, i + 1, fnCallback);
      }, 150); // text finished, call callback if there is a callback function);
    } else if (typeof fnCallback == "function") {
      // call callback after timeout
      setTimeout(fnCallback, 2000);
    }
  }
  // start a typewriter animation for a text in the dataText array
  function StartTextAnimation(i) {
    if (paused) {
      breakWords();
      return;
    }
    if (typeof dataText[i] == "undefined") {
      setTimeout(function() {
        StartTextAnimation(0);
      }, 2000);
    }
    // check if dataText[i] exists
    if (i < dataText[i].length) {
      // text exists! start typewriter animation
      typeWriter(dataText[i], 0, function() {
        // after callback (and whole text has been animated), start next text
        StartTextAnimation(i + 1);
      });
    }
  }
  // start the text animation
  if (!paused) {
    initWords();
    StartTextAnimation(0);
  }

  var options = {
    accessibility: true,
    adaptiveHeight: false,
    autoPlay: false,
    cellAlign: "center",
    cellSelector: undefined,
    contain: false,
    draggable: ">1",
    dragThreshold: 3,
    freeScroll: false,
    selectedAttraction: 0.075,
    friction: 0.8,
    groupCells: false,
    initialIndex: 0,
    lazyLoad: true,
    percentPosition: true,
    prevNextButtons: true,
    pageDots: false,
    resize: true,
    rightToLeft: false,
    setGallerySize: true,
    watchCSS: false,
    wrapAround: false
  };
  var carousels = document.querySelectorAll('[data-target="carousel"]');
  var carouselArray = [];
  for (var i = 0; i < carousels.length; i++) {
    var carousel = carousels[i];
    new Flickity(carousel, options);
  }

  // var slider = tns({
  //     container: '.carousel',
  //     items: 2,
  //     center: true,
  //     slideBy: 1,
  //     autoplay: false,
  //     arrowKeys: true,
  //     nav: false,
  //     fixedWidth: 800,
  //     controls: false,
  //     loop: false
  //   });

  //   var slider2 = tns({
  //     container: '.carousel2',
  //     items: 2,
  //     center: true,
  //     slideBy: 1,
  //     autoplay: false,
  //     arrowKeys: true,
  //     nav: false,
  //     fixedWidth: 800,
  //     controls: false,
  //     loop: false
  //   });

  var waypoints = [];
  var thingstoanimate = document.querySelectorAll(".animate");
  thingstoanimate.forEach(function(currentValue, currentIndex, listObj) {
    console.log(currentValue);
    // hide all the items
    for (var i = 0; i < currentValue.children.length; i++) {
      currentValue.children[i].classList.add("hide");
    }

    // create waypoints to show them
    waypoints.push(
      new Waypoint({
        element: currentValue,
        handler: function(direction) {
          console.log(this);
          var list = this.element.children;
          var timer;
          for (var i = 0; i < list.length; i++) {
            if (direction == "down") {
              list[i].classList.remove("hide");
              list[i].classList.add("fade-in-fwd");
            } else {
              list[i].classList.add("hide");
              list[i].classList.remove("fade-in-fwd");
            }
          }
        },
        offset: "100%"
      })
    );
  });
});
var players = [];
// $(".infographic").click(function() {
//   window.location = $(this)
//     .find("a")
//     .attr("href");
// });

$("[data-video]").each(function(i, el) {
  // console.log("EL");
  // console.log(el);
  // console.log("I");
  // console.log(i);
  var vid = $(this).attr("data-video");
  $("#video-modals").append(
    '<div id="video-' +
      vid +
      '" data-vimeo-id="' +
      vid +
      '" class="video--16x9" data-video-index="' +
      i +
      '"></div>'
  );

  players[vid] = new Vimeo.Player("video-" + vid);
});

$(".play-video").each(function(i, el) {
  var vid = $(el)
    .parent()
    .attr("data-video");

  var starttime = $(el)
    .parent()
    .attr("data-start-time");

  $(this).click(function(e) {
    $("#video-modals").addClass("show");
    $("#video-" + vid).addClass("show");
    if (starttime) {
      players[vid].play().then(function() {
        players[vid].setCurrentTime(starttime);
      });
    } else {
      players[vid].play();
    }
  });
});

$(".close-modal, #video-modals").click(function() {
  $("#video-modals").removeClass("show");
  $("[data-vimeo-id]").each(function() {
    players[$(this).attr("data-vimeo-id")].pause();
    $(this).removeClass("show");
  });
});

/**
 * forEach implementation for Objects/NodeLists/Arrays, automatic type loops and context options
 *
 * @private
 * @author Todd Motto
 * @link https://github.com/toddmotto/foreach
 * @param {Array|Object|NodeList} collection - Collection of items to iterate, could be an Array, Object or NodeList
 * @callback requestCallback      callback   - Callback function for each iteration.
 * @param {Array|Object|NodeList} scope=null - Object/NodeList/Array that forEach is iterating over, to use as the this value when executing callback.
 * @returns {}
 */
var forEach = function(t, o, r) {
  if ("[object Object]" === Object.prototype.toString.call(t))
    for (var c in t)
      Object.prototype.hasOwnProperty.call(t, c) && o.call(r, t[c], c, t);
  else for (var e = 0, l = t.length; l > e; e++) o.call(r, t[e], e, t);
};
var hamburgers = document.querySelectorAll(".hamburger");
var nav = document.querySelector("nav");
if (hamburgers.length > 0) {
  forEach(hamburgers, function(hamburger) {
    hamburger.addEventListener(
      "click",
      function() {
        this.classList.toggle("is-active");
        nav.classList.toggle("is-active");
      },
      false
    );
  });
}
