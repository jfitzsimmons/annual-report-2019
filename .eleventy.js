const inputDir = `src/site`;
const componentsDir = `${inputDir}/_includes/components`;

// Components
const FullSection = require(`./${componentsDir}/FullSection.js`);
const Section = require(`./${componentsDir}/Section.js`);
const Image = require(`./${componentsDir}/Image.js`);
const Button = require(`./${componentsDir}/Button.js`);
const ContentCard = require(`./${componentsDir}/ContentCard.js`);
const Infographic = require(`./${componentsDir}/Infographic.js`);

module.exports = function(config) {
  // Paired shortcodes
  config.addPairedShortcode("Section", Section);
  config.addPairedShortcode("FullSection", FullSection);

  // Shortcodes
  config.addShortcode("Image", Image);
  config.addShortcode("Infographic", Infographic);
  config.addShortcode("Button", Button);
  config.addShortcode("ContentCard", ContentCard);

  return {
    // pathPrefix: "/annual-report-2019/",
    dir: {
      input: inputDir,
      output: "dist"
    },
    passthroughFileCopy: true,
    templateFormats: [
      "html",
      "liquid",
      "ejs",
      "md",
      "hbs",
      "mustache",
      "haml",
      "pug",
      "njk",
      "11ty.js",
      "css",
      "js",
      "svg",
      "jpg"
    ]
  };
};
