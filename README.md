# Getting started

Clone the repo to a folder on your machine. Make sure you have write access to the bitbucket repo. Open the project in your code editor such as VS Code.

Run `npm run start` to build and serve the project and also to begin having sass watch for .scss changes.

** Updating the footer or main navigation links **

Page names and urls that are used in the navigation blocks are located within `src/site/_data/pages.json`. For example, to change the text of a link in the footer or the main navigation, edit this file to have it update across the site.

** Updating page content **

Page content is located within `src/site/<section name>`. For example, if you'd like to change the content on the Grand Challenges section, you'd need to edit `src/site/grand-challenges/index.njk`

Upon saving, the site will automatically be rebuilt and available at localhost:8080.

** Deploying **

`git add .`

`git commit -m "Changes"`

`git push origin master`

Upon pushing the master branch to the origin remote, it will trigger a build on AWS CodeBuild and automatically update https://www.oswego.edu/annual-report-2019/index.html

# eleventy-shortcomps documentation

Starter project for static site, using [Eleventy](https://11ty.io) and shortcode components (AKA _shortcomps_) pattern.

## Goal

The ability to create and maintain reusable, framework-agnostic [functional stateless components](https://javascriptplayground.com/functional-stateless-components-react/).

These can be used throughout static sites and/or in environments already utilising frameworks (e.g. React, Vue). They are composable, and serve as the single source of truth across all applications.

Benefit from the advantages of the component model, without needing to reach for a framework right away.

## Concept

As in many frameworks, components can be written as functions that return JavaScript Template Literals. These receive and interpolate any values passed as arguments, and can contain rendering logic:

```JavaScript
// Button.js
module.exports = (text, href, primary) => {

  // rendering logic, classnames etc.?
  const primaryClass = primary ? 'button--primary' : '';

  return `
    <a class="button ${ primaryClass }" href="${ href }">
      ${ text }
    </a>
  `;
};
```

Import and define components using Eleventy’s `addShortcode` and `addPairedShortcode` config methods, as needed:

```JavaScript
// .eleventy.js
const componentsDir = `./_includes/components`;

const Wrapper = require(`${ componentsDir }/Wrapper.js`);
const Card = require(`${ componentsDir }/Card.js`);
const Button = require(`${ componentsDir }/Button.js`);

module.exports = function (config) {

  config.addPairedShortcode('Wrapper', Wrapper);
  config.addShortcode('Card', Card);
  config.addShortcode('Button', Button);

};
```

They’ll then be available throughout templates, using the include syntax (i.e. Nunjucks):

```HTML
{% Button 'This is a link to Eleventy', 'http://11ty.io' %}
```

And can be nested within other components:

```JavaScript
// Card.js
const Button = require('./Button.js');

module.exports = (name, bio, url) => (`
  <article class="card">
    <h3>${ name }</h3>
    <p>${ bio }</p>

    ${ Button('Visit site', url) }
  </article>
`);
```

## Props variation

Developers coming from (or possibly heading towards) a framework-based component model might be used to passing and receiving their component parameters in a single `props` object.

It’s an elegant way of saying, “Hey, component, here’s everything you’ll need in one tasty little package.”

This commonly results in a functional component that looks more like:

```JavaScript
// Image.js
module.exports = ({ src, altText = '', caption = '' }) => (`
  <figure class="media">
    <img src="${ src }" alt="${ altText }">
    ${ caption && `
      <figcaption>${ caption }</figcaption>
    `}
  </figure>
`);
```

(See React’s [Functional and class components](https://reactjs.org/docs/components-and-props.html#functional-and-class-components) documentation)

This single `props` argument can also be [destructured](https://davidwalsh.name/destructuring-function-arguments) and assigned [default parameter values](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Default_parameters). Awesome.

_Note:_ The example above uses the [logical AND operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Logical_Operators#Description) to add conditional rendering logic for the `<figcaption>`. Ensure props have default values set to avoid this rendering a value of `false` in compiled templates.

With this approach, we still declare our shortcodes in `.eleventy.js` as we did previously. But instead of passing multiple parameters to them in our templates, we pass a single object containing all of the properties. In a templating language like Nunjucks, that might look like:

```HTML
{% Image {
  src: '/path/to/image.jpg',
  altText: 'The Beatles on stage at Shea Stadium',
  caption: 'Where’s Ringo?'
} %}
```

Or, if you’re using a functional component inside another component, that could start to look a whole lot like those React’y components:

```JavaScript
// SomeComponent.js
const Image = require('./Image.js');

module.exports = ({ title, image = {} } = {}) => {
  const { src, altText = '', caption } = image;

  return `
    <div class="some-component">
      ${ title && `
        <h2>${ title }</h2>
      `}
      ${ Image({
        src,
        altText,
        caption
      }) }
    </div>
  `;
};
```

Here we can further leverage modern JavaScript features, such as [Object property value shorthand](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#New_notations_in_ECMAScript_2015) — along with destructuring — to give us a clear, terse syntax for using components.

It seems advantageous to use this `props` approach in favour of the multiple parameter approach outlined first. Our components will benefit from having the same functional signatures as their React (and to some degree, Vue) counterparts, should we need to take them there in the future.

## Demo

This repo contains just enough to demonstrate how one _could_ utilise this pattern (config, functional stateless components, props, shortcodes, paired shortcodes, layouts).

Site can be viewed at: [eleventy-shortcomps.netlify.com](https://eleventy-shortcomps.netlify.com)

Feedback welcome 🙌
